#!/bin/bash

if [ ! -f $HOME/columbus_libs_setup.sh ]; then
    echo "Setting default library locations."
    export PDK_ROOT=$HOME/pdks/sky130A
else
    echo "Sourcing \"$HOME/columbus_libs_setup.sh\""
    source $HOME/columbus_libs_setup.sh
fi

#export XSCHEM_ROOT=/usr/local/share/xschem

export PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export ANALOGLIB_ROOT=$PROJECT_ROOT/analoglib

alias sch='$PROJECT_ROOT/schematic.sh'
alias lay='$PROJECT_ROOT/layout.sh'
alias wrapper='lay top user_analog_project_wrapper'
#magic -d OGL -rcfile $PDK_ROOT/sky130A/libs.tech/magic/sky130A.magicrc $1
alias out='$PROJECT_ROOT/out.sh'
alias lvs='$PROJECT_ROOT/lvs.sh'
alias pex='$PROJECT_ROOT/pex.sh'
alias rpt='less $(cat $PROJECT_ROOT/lastreport)'
alias net='less $(cat $PROJECT_ROOT/lastpex)'

echo "Analog Design Environment Setup Complete!"
