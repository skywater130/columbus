# About
The ultimate goal of this project is to build an open source narrow band tranciever chip.  The project is just getting started and this test chip is designed to test various analog IP blocks built using the Skywater 130nm PDK.  So far this test chip includes the following IP blocks:

The blocks:

[Bandgap refrence](ip/bgr/docs/README.md)

[Load Switch](ip/pswitch/docs/README.md)

[LDO](ip/ldo/docs/README.md)

The design is located in the `columbus` git submodule (where this readme is located).  The blocks are organised under the `ip` folder by name, and each block contains a `sch` folder and a `lay` folder.  The ip blocks follow a naming convention in which each file in a block has the block name as a prefix, where the top level of each design is called `<block-name>_top.xxx`.

There is a file called `envsetup.sh` in the columbus folder which sets up the environment.  Refer to that for an indication of which environment variables need to be defined etc.

# Creating a new block
To Create a new IP block, simply create a folder whos name is the name of the IP block, then add the corrasponding `sch` and `lay` sub folders.

# Running a simulation
To run a simulation, run xschem by typing `sch` at the terminal (assuming you've sourced the envsetup.sh bash script) and open one of the test schematics e.g. `ip/bgr/sch/tests/bgr_top_test_tran.sch` and control-right-click on the spice card.
