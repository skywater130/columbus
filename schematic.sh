#!/bin/bash

# open top level schematic view of provided cell else design top
if [ $# -lt 1 ]; then
    SCHEMATIC_FILE_NAME=$PROJECT_ROOT/ip/top/sch/top.sch
else
    SCHEMATIC_FILE_NAME=$PROJECT_ROOT/ip/$1/sch/$1_top.sch
fi

if [ ! -f $SCHEMATIC_FILE_NAME ]; then
    echo "No Schematic called \"$SCHEMATIC_FILE_NAME\"."
    exit
fi

xschem -b --rcfile $PROJECT_ROOT/xschemrc $SCHEMATIC_FILE_NAME
