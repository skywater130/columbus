# Scailable Power Switch With Inbuilt 'EN' Level-Translator

This load switch is used to switch the high-side supply to other circuits on or of.  It has two power inputs `vddc` and `vddp`, one enable `en` and the switched output `vddp_sw`.  The enable signal is refrenced to `vddc` and can operate down to 1.8V indapendant of the switched supply domain.  The enable signal is active high and when asserted will connect the output power rail `vddp_sw` to `vddp`.

The load switch is scalable by abutting the layouts horazontally (see layout below) to switch greater currents.
![alt text](img/lay_pswitch_top.png "Power Switch Slice Layout")
