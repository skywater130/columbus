v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 240 -500 240 -410 { lab=en}
N 280 -680 280 -660 { lab=vddc}
N 280 -380 280 -360 { lab=vss}
N 470 -680 470 -660 { lab=vddc}
N 470 -380 470 -360 { lab=vss}
N 280 -410 300 -410 { lab=vss}
N 300 -410 300 -360 { lab=vss}
N 280 -360 300 -360 { lab=vss}
N 280 -630 300 -630 { lab=vddc}
N 300 -680 300 -630 { lab=vddc}
N 280 -680 300 -680 { lab=vddc}
N 470 -630 490 -630 { lab=vddc}
N 490 -680 490 -630 { lab=vddc}
N 470 -680 490 -680 { lab=vddc}
N 470 -410 490 -410 { lab=vss}
N 490 -410 490 -360 { lab=vss}
N 470 -360 490 -360 { lab=vss}
N 300 -360 470 -360 { lab=vss}
N 300 -680 470 -680 { lab=vddc}
N 720 -380 720 -360 { lab=vss}
N 490 -360 720 -360 { lab=vss}
N 910 -380 910 -360 { lab=vss}
N 740 -360 910 -360 { lab=vss}
N 720 -410 740 -410 { lab=vss}
N 740 -410 740 -360 { lab=vss}
N 720 -360 740 -360 { lab=vss}
N 910 -410 930 -410 { lab=vss}
N 930 -410 930 -360 { lab=vss}
N 910 -360 930 -360 { lab=vss}
N 760 -630 760 -580 { lab=vo}
N 870 -630 870 -580 { lab=d1}
N 760 -540 870 -580 { lab=d1}
N 760 -580 870 -540 { lab=vo}
N 870 -540 910 -540 { lab=vo}
N 910 -560 910 -540 { lab=vo}
N 720 -540 760 -540 { lab=d1}
N 720 -600 720 -540 { lab=d1}
N 720 -540 720 -440 { lab=d1}
N 720 -680 720 -660 { lab=vddp}
N 700 -680 720 -680 { lab=vddp}
N 720 -680 910 -680 { lab=vddp}
N 910 -680 910 -660 { lab=vddp}
N 700 -630 720 -630 { lab=vddp}
N 700 -680 700 -630 { lab=vddp}
N 910 -630 930 -630 { lab=vddp}
N 930 -680 930 -630 { lab=vddp}
N 910 -680 930 -680 { lab=vddp}
N 470 -500 470 -440 { lab=g2}
N 910 -540 910 -440 { lab=vo}
N 280 -520 280 -440 { lab=g1}
N 430 -480 430 -410 { lab=g1}
N 430 -480 680 -480 { lab=g1}
N 680 -480 680 -410 { lab=g1}
N 430 -520 430 -480 { lab=g1}
N 470 -500 870 -500 { lab=g2}
N 870 -500 870 -410 { lab=g2}
N 470 -600 470 -500 { lab=g2}
N 280 -520 430 -520 { lab=g1}
N 280 -600 280 -520 { lab=g1}
N 430 -630 430 -520 { lab=g1}
N 930 -680 1090 -680 { lab=vddp}
N 1090 -630 1110 -630 { lab=vddp}
N 1110 -680 1110 -630 { lab=vddp}
N 1090 -680 1110 -680 { lab=vddp}
N 1090 -680 1090 -660 { lab=vddp}
N 1050 -630 1050 -560 { lab=vo}
N 910 -560 1050 -560 { lab=vo}
N 910 -600 910 -560 { lab=vo}
N 280 -710 280 -680 { lab=vddc}
N 700 -710 700 -680 { lab=vddp}
N 210 -500 240 -500 { lab=en}
N 240 -630 240 -500 { lab=en}
N 210 -360 280 -360 { lab=vss}
N 1090 -600 1090 -580 { lab=vddp_sw}
C {common/border.sym} 0 0 0 0 {design_name="Power Switch"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 260 -410 0 0 {name=M1
L=0.5
W=2
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 260 -630 0 0 {name=M2
L=0.5
W=6
nf=6
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 450 -410 0 0 {name=M3
L=0.5
W=2
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 450 -630 0 0 {name=M4
L=0.5
W=6
nf=6
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 700 -410 0 0 {name=M5
L=0.5
W=8
nf=8
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 890 -410 0 0 {name=M6
L=0.5
W=8
nf=8
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 740 -630 0 1 {name=M7
L=0.5
W=4
nf=4
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 890 -630 0 0 {name=M8
L=0.5
W=4
nf=4
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/lab_pin.sym} 910 -520 0 1 {name=l2 sig_type=std_logic lab=vo}
C {devices/lab_pin.sym} 680 -480 0 1 {name=l6 sig_type=std_logic lab=g1}
C {devices/lab_pin.sym} 870 -480 0 1 {name=l7 sig_type=std_logic lab=g2}
C {devices/lab_pin.sym} 720 -520 0 0 {name=l8 sig_type=std_logic lab=d1}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1070 -630 0 0 {name=M9
L=0.5
W=260
nf=26
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/iopin.sym} 1090 -580 1 0 {name=p1 lab=vddp_sw}
C {devices/iopin.sym} 280 -710 3 0 {name=p2 lab=vddc}
C {devices/iopin.sym} 700 -710 3 0 {name=p3 lab=vddp}
C {devices/iopin.sym} 210 -360 2 0 {name=p4 lab=vss}
C {devices/iopin.sym} 210 -500 2 0 {name=p5 lab=en}
