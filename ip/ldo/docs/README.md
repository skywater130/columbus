# Linear Regulator

![alt text](img/sym_ldo_top.png "Linear Regulator Layout")

![alt text](img/sch_ldo_top.png "Linear Regulator Layout")

![alt text](img/sch_ldo_base.png "Linear Regulator Layout")

![alt text](img/sch_ldo_amp.png "Linear Regulator Layout")

![alt text](img/lay_ldo_top.png "Linear Regulator Layout")
