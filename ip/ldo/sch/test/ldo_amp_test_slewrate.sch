v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 960 -670 960 -540 { lab=vdd}
N 80 -490 80 -130 { lab=0}
N 80 -670 80 -550 { lab=vdd}
N 930 -670 960 -670 { lab=vdd}
N 710 -130 960 -130 { lab=0}
N 960 -460 960 -130 { lab=0}
N 930 -580 930 -540 { lab=#net1}
N 930 -670 930 -640 { lab=vdd}
N 1170 -500 1170 -340 { lab=vout}
N 1170 -280 1170 -130 { lab=0}
N 960 -130 1170 -130 { lab=0}
N 1000 -500 1170 -500 { lab=vout}
N 780 -500 780 -480 { lab=#net2}
N 680 -500 780 -500 { lab=#net2}
N 680 -500 680 -480 { lab=#net2}
N 810 -480 920 -480 { lab=vin}
N 460 -500 680 -500 { lab=#net2}
N 460 -500 460 -480 { lab=#net2}
N 460 -420 460 -400 { lab=0}
N 460 -400 680 -400 { lab=0}
N 680 -420 680 -400 { lab=0}
N 680 -400 780 -400 { lab=0}
N 780 -420 780 -400 { lab=0}
N 710 -420 710 -380 { lab=#net3}
N 710 -380 810 -380 { lab=#net3}
N 810 -420 810 -380 { lab=#net3}
N 710 -520 710 -480 { lab=vip}
N 710 -520 920 -520 { lab=vip}
N 710 -380 710 -320 { lab=#net3}
N 710 -260 710 -130 { lab=0}
N 460 -130 710 -130 { lab=0}
N 80 -670 930 -670 { lab=vdd}
N 460 -400 460 -130 { lab=0}
N 80 -130 460 -130 { lab=0}
C {devices/lab_pin.sym} 80 -670 0 0 {name=l1 sig_type=std_logic lab=vdd}
C {devices/gnd.sym} 80 -130 0 0 {name=l4 lab=0}
C {vsource.sym} 80 -520 0 0 {
name=V1
value="dc 3.3"
}
C {vsource.sym} 710 -290 0 0 {
name=V2
value="dc 1.65"
}
C {devices/lab_pin.sym} 1130 -500 1 0 {name=l3 sig_type=std_logic lab=vout}
C {cap.sym} 1170 -310 0 0 {
name=C1
value=1f
}
C {vsource.sym} 460 -450 0 0 {
name=V4
value="dc 0 pwl(0 0 1000f 0 1001f 1)"
}
C {isource.sym} 930 -610 0 1 {
name=I2
value="dc 25u"
}
C {devices/lab_pin.sym} 900 -520 1 0 {name=l2 sig_type=std_logic lab=vip}
C {vcvs.sym} 710 -450 0 0 {
name=E1
gain=0.5
}
C {vcvs.sym} 810 -450 0 0 {
name=E2
gain=-0.5
}
C {devices/lab_pin.sym} 900 -480 3 0 {name=l5 sig_type=std_logic lab=vin}
C {ldo/sch/ldo_amp.sym} 960 -500 0 0 {
name=I1
}
C {common/sky130libs.sym} 1420 -880 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {common/spice.sym} 1340 -880 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

tran 1f 1n
plot vout vip vin

.endc
"
}
