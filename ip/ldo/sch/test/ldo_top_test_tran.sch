v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 80 -560 80 -200 { lab=0}
N 80 -740 80 -620 { lab=vdd}
N 700 -470 700 -420 { lab=#net1}
N 700 -360 700 -200 { lab=0}
N 1040 -360 1350 -360 { lab=vlc}
N 1380 -300 1380 -200 { lab=0}
N 760 -200 1380 -200 { lab=0}
N 700 -470 760 -470 { lab=#net1}
N 1040 -300 1380 -300 { lab=0}
N 700 -200 760 -200 { lab=0}
N 1380 -470 1380 -360 { lab=v2}
N 760 -470 920 -470 { lab=#net1}
N 660 -470 700 -470 { lab=#net1}
N 350 -550 350 -510 { lab=#net2}
N 760 -230 760 -200 { lab=0}
N 760 -470 760 -290 { lab=#net1}
N 180 -310 180 -200 { lab=0}
N 80 -200 180 -200 { lab=0}
N 320 -740 350 -740 { lab=vdd}
N 180 -490 270 -490 { lab=#net3}
N 320 -200 700 -200 { lab=0}
N 350 -740 350 -610 { lab=vdd}
N 320 -740 320 -510 { lab=vdd}
N 80 -740 320 -740 { lab=vdd}
N 1240 -470 1380 -470 { lab=v2}
N 980 -470 1180 -470 { lab=v1}
N 180 -490 180 -370 { lab=#net3}
N 540 -470 600 -470 { lab=vreg}
N 320 -430 320 -200 { lab=0}
N 180 -200 320 -200 { lab=0}
N 240 -450 270 -450 { lab=vreg}
N 240 -450 240 -380 { lab=vreg}
N 240 -380 540 -380 { lab=vreg}
N 540 -470 540 -380 { lab=vreg}
N 370 -470 540 -470 { lab=vreg}
C {devices/lab_pin.sym} 80 -740 0 0 {name=l1 sig_type=std_logic lab=vdd}
C {devices/gnd.sym} 80 -200 0 0 {name=l4 lab=0}
C {vsource.sym} 80 -590 0 0 {
name=V1
value="dc 3.3"
}
C {vsource.sym} 180 -340 0 0 {
name=V2
value="dc 1.8"
}
C {res.sym} 700 -390 0 0 {
name=R1
value=1k
}
C {devices/lab_pin.sym} 500 -470 1 0 {name=l2 sig_type=std_logic lab=vreg}
C {vsource.sym} 1040 -330 0 0 {
name=V3
value="dc -2 sin(0 1 100meg 0 0 270)"
}
C {vcsw.sym} 1380 -330 0 0 {
name=S1
spiceprefix=X
ron=1m
roff=1meg
}
C {res.sym} 1270 -330 0 0 {
name=R2
value=1k
}
C {devices/lab_pin.sym} 1180 -360 1 0 {name=l5 sig_type=std_logic lab=vlc}
C {cap.sym} 760 -260 0 0 {
name=C1
value=1p
}
C {res.sym} 1210 -470 1 0 {
name=R3
value=1k8
}
C {res.sym} 950 -470 1 0 {
name=R4
value=60m
}
C {vsource.sym} 630 -470 3 0 {
name=V5
value="dc 0"
}
C {isource.sym} 350 -580 0 0 {
name=I2
value="dc 25u"
}
C {devices/lab_pin.sym} 1140 -470 1 0 {name=l6 sig_type=std_logic lab=v1}
C {devices/lab_pin.sym} 1320 -470 1 0 {name=l3 sig_type=std_logic lab=v2}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {common/spice.sym} 1500 -760 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

*ac dec 1000 1 100gig
*let phase=ph(ac1.vreg)*180/pi
*plot vdb(ac1.vreg) phase xlog
*meas ac gain_marg find vdb(vreg) when phase=0
*meas ac phase_marg find phase when vdb(vreg)=0
*meas ac ft find phase when vdb(vreg)=0
*print -gain_marg
*print phase_marg

tran 1p 10n
plot v(tran1.xi1.vg) v(tran1.vreg) v(tran1.v1) v(tran1.v2)
*plot i(v5)

*op

*let cgg1=@m.xm1.msky130_fd_pr__nfet_05v0_nvt[cgg]
*let rs1=1/@m.xm1.msky130_fd_pr__nfet_05v0_nvt[gm]
*print v(vreg) cgg1 rs1
write ldo_top.raw

.endc
"
}
C {ldo/sch/ldo_base.sym} 320 -470 0 0 {
name=I1
}
C {common/border.sym} 0 0 0 0 {design_name="design name"
revision="00"
author="author"
name="border"
}
