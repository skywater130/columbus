v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 400 -400 820 -400 { lab=vreg}
N 400 -480 400 -400 { lab=vreg}
N 210 -520 490 -520 { lab=#net1}
N 540 -130 980 -130 { lab=0}
N 110 -490 110 -130 { lab=0}
N 110 -670 110 -550 { lab=vdd}
N 210 -520 210 -470 { lab=#net1}
N 210 -410 210 -130 { lab=0}
N 110 -130 210 -130 { lab=0}
N 980 -400 980 -350 { lab=#net2}
N 980 -290 980 -130 { lab=0}
N 1320 -290 1630 -290 { lab=vlc}
N 1660 -230 1660 -130 { lab=0}
N 1040 -130 1660 -130 { lab=0}
N 980 -400 1040 -400 { lab=#net2}
N 1320 -230 1660 -230 { lab=0}
N 980 -130 1040 -130 { lab=0}
N 1660 -400 1660 -350 { lab=v1}
N 1040 -400 1200 -400 { lab=#net2}
N 1260 -400 1660 -400 { lab=v1}
N 210 -580 300 -580 { lab=#net3}
N 300 -580 300 -560 { lab=#net3}
N 300 -560 400 -560 { lab=#net3}
N 300 -560 300 -380 { lab=#net3}
N 300 -320 300 -130 { lab=0}
N 210 -130 300 -130 { lab=0}
N 820 -400 880 -400 { lab=vreg}
N 940 -400 980 -400 { lab=#net2}
N 570 -580 570 -540 { lab=#net4}
N 570 -670 570 -640 { lab=vdd}
N 1040 -160 1040 -130 { lab=0}
N 1040 -400 1040 -220 { lab=#net2}
N 400 -480 490 -480 { lab=vreg}
N 540 -670 570 -670 { lab=vdd}
N 540 -670 540 -540 { lab=vdd}
N 110 -670 540 -670 { lab=vdd}
N 820 -500 820 -400 { lab=vreg}
N 590 -500 820 -500 { lab=vreg}
N 540 -460 540 -130 { lab=0}
N 300 -130 540 -130 { lab=0}
C {devices/lab_pin.sym} 110 -670 0 0 {name=l1 sig_type=std_logic lab=vdd}
C {devices/gnd.sym} 320 -130 0 0 {name=l4 lab=0}
C {vsource.sym} 110 -520 0 0 {
name=V1
value="dc 3.3"
}
C {vsource.sym} 210 -440 0 0 {
name=V2
value="dc 1.8"
}
C {res.sym} 980 -320 0 0 {
name=R1
value=1k
}
C {devices/lab_pin.sym} 700 -400 1 0 {name=l2 sig_type=std_logic lab=vreg}
C {vsource.sym} 1320 -260 0 0 {
name=V3
value="dc -2 sin(0 1 1meg 0 0 270)"
}
C {vcsw.sym} 1660 -260 0 0 {
name=S1
spiceprefix=X
ron=1m
roff=1meg
}
C {res.sym} 1550 -260 0 0 {
name=R2
value=1k
}
C {devices/lab_pin.sym} 1460 -290 1 0 {name=l5 sig_type=std_logic lab=vlc}
C {cap.sym} 1040 -190 0 0 {
name=C1
value=1p
}
C {res.sym} 1660 -320 0 0 {
name=R3
value=60
}
C {res.sym} 1230 -400 1 0 {
name=R4
value=60m
}
C {vsource.sym} 210 -550 0 0 {
name=V4
value="dc 0 ac 1"
}
C {res.sym} 300 -350 0 0 {
name=R5
value=1k
}
C {vsource.sym} 910 -400 3 0 {
name=V5
value="dc 0"
}
C {isource.sym} 570 -610 0 0 {
name=I2
value="dc 25u"
}
C {devices/lab_pin.sym} 1420 -400 1 0 {name=l6 sig_type=std_logic lab=v1}
C {ldo/sch/ldo_base.sym} 540 -500 0 0 {
name=I3
}
C {common/sky130libs.sym} 1920 -960 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {common/spice.sym} 1840 -960 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

ac dec 1000 1 100gig
let phase=ph(ac1.vreg)*180/pi
plot vdb(ac1.vreg) phase xlog
meas ac gain_marg find vdb(vreg) when phase=0
meas ac phase_marg find phase when vdb(vreg)=0
*meas ac ft find phase when vdb(vreg)=0
print -gain_marg
print phase_marg

tran 1n 1u
plot v(tran1.vreg) v(tran1.v1)
plot i(v5)

*op

*let cgg1=@m.xm1.msky130_fd_pr__nfet_05v0_nvt[cgg]
*let rs1=1/@m.xm1.msky130_fd_pr__nfet_05v0_nvt[gm]
*print v(vreg) cgg1 rs1
write ldo_top.raw

.endc
"
}
