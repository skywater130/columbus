v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 530 -670 530 -540 { lab=vdd}
N 400 -480 490 -480 { lab=#net1}
N 210 -520 490 -520 { lab=#net2}
N 110 -490 110 -130 { lab=0}
N 110 -670 110 -550 { lab=vdd}
N 500 -670 530 -670 { lab=vdd}
N 210 -520 210 -470 { lab=#net2}
N 110 -130 210 -130 { lab=0}
N 530 -460 530 -130 { lab=0}
N 210 -130 530 -130 { lab=0}
N 500 -580 500 -540 { lab=#net3}
N 500 -670 500 -640 { lab=vdd}
N 110 -670 500 -670 { lab=vdd}
N 740 -500 740 -340 { lab=vout}
N 740 -280 740 -130 { lab=0}
N 530 -130 740 -130 { lab=0}
N 400 -480 400 -370 { lab=#net1}
N 210 -370 400 -370 { lab=#net1}
N 210 -410 210 -370 { lab=#net1}
N 210 -370 210 -340 { lab=#net1}
N 210 -280 210 -130 { lab=0}
N 570 -500 740 -500 { lab=vout}
C {devices/lab_pin.sym} 110 -670 0 0 {name=l1 sig_type=std_logic lab=vdd}
C {devices/gnd.sym} 320 -130 0 0 {name=l4 lab=0}
C {vsource.sym} 110 -520 0 0 {
name=V1
value="dc 3.3"
}
C {vsource.sym} 210 -310 0 0 {
name=V2
value="dc 1.65"
}
C {devices/lab_pin.sym} 700 -500 1 0 {name=l3 sig_type=std_logic lab=vout}
C {cap.sym} 740 -310 0 0 {
name=C1
value=1p
}
C {vsource.sym} 210 -440 0 0 {
name=V4
value="dc 0 ac 1 180"
}
C {isource.sym} 500 -610 0 1 {
name=I2
value="dc 25u"
}
C {ldo/sch/ldo_amp.sym} 530 -500 0 0 {
name=I1
}
C {common/sky130libs.sym} 980 -820 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {common/spice.sym} 900 -820 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

ac dec 1000 1 100gig
let phase=ph(ac1.vout)*180/pi
plot vdb(ac1.vout) phase xlog
meas ac gain_marg find vdb(vout) when phase=0
meas ac phase_marg find phase when vdb(vout)=0
*meas ac ft find phase when vdb(vout)=0
print -gain_marg
print phase_marg

write ldo_amp_test_ac.raw

.endc
"
}
