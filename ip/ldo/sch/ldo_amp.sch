v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
T {100uA} 570 -1010 0 0 0.4 0.4 {}
T {100uA} 790 -1010 0 0 0.4 0.4 {}
N 570 -640 590 -640 { lab=vss}
N 590 -640 590 -270 { lab=vss}
N 710 -270 810 -270 { lab=vss}
N 790 -640 810 -640 { lab=vss}
N 810 -640 810 -270 { lab=vss}
N 690 -500 790 -500 { lab=#net1}
N 570 -610 570 -500 { lab=#net1}
N 790 -610 790 -500 { lab=#net1}
N 590 -270 690 -270 { lab=vss}
N 570 -500 690 -500 { lab=#net1}
N 570 -820 570 -670 { lab=#net2}
N 790 -760 790 -670 { lab=vo}
N 570 -890 590 -890 { lab=vdd}
N 590 -980 590 -890 { lab=vdd}
N 570 -980 590 -980 { lab=vdd}
N 590 -980 790 -980 { lab=vdd}
N 790 -980 790 -920 { lab=vdd}
N 790 -980 810 -980 { lab=vdd}
N 810 -980 810 -890 { lab=vdd}
N 790 -890 810 -890 { lab=vdd}
N 530 -890 530 -820 { lab=#net2}
N 570 -980 570 -920 { lab=vdd}
N 690 -500 690 -400 { lab=#net1}
N 690 -340 690 -270 { lab=vss}
N 750 -890 750 -840 { lab=#net2}
N 570 -840 750 -840 { lab=#net2}
N 530 -820 570 -820 { lab=#net2}
N 570 -860 570 -840 { lab=#net2}
N 570 -840 570 -820 { lab=#net2}
N 350 -270 590 -270 { lab=vss}
N 790 -860 790 -760 { lab=vo}
N 790 -760 890 -760 { lab=vo}
N 750 -640 750 -580 { lab=vin}
N 690 -370 710 -370 { lab=vss}
N 710 -370 710 -270 { lab=vss}
N 330 -340 330 -270 { lab=vss}
N 330 -370 350 -370 { lab=vss}
N 350 -370 350 -270 { lab=vss}
N 650 -420 650 -370 { lab=ir25u}
N 330 -420 650 -420 { lab=ir25u}
N 330 -420 330 -400 { lab=ir25u}
N 690 -270 710 -270 { lab=vss}
N 180 -270 330 -270 { lab=vss}
N 330 -270 350 -270 { lab=vss}
N 330 -440 330 -420 { lab=ir25u}
N 290 -440 330 -440 { lab=ir25u}
N 290 -440 290 -370 { lab=ir25u}
N 470 -580 750 -580 { lab=vin}
N 470 -640 530 -640 { lab=vip}
N 430 -980 570 -980 { lab=vdd}
N 330 -480 330 -440 { lab=ir25u}
N 370 -980 370 -890 { lab=vdd}
N 180 -980 370 -980 { lab=vdd}
N 410 -890 430 -890 { lab=vdd}
N 430 -980 430 -890 { lab=vdd}
N 410 -980 430 -980 { lab=vdd}
N 410 -980 410 -920 { lab=vdd}
N 370 -980 410 -980 { lab=vdd}
N 850 -370 850 -270 { lab=vss}
N 810 -270 850 -270 { lab=vss}
N 890 -340 890 -270 { lab=vss}
N 850 -270 890 -270 { lab=vss}
N 890 -370 910 -370 { lab=vss}
N 910 -370 910 -270 { lab=vss}
N 890 -270 910 -270 { lab=vss}
N 790 -500 1090 -500 { lab=#net1}
N 1090 -340 1090 -270 { lab=vss}
N 1050 -270 1090 -270 { lab=vss}
N 1050 -370 1050 -270 { lab=vss}
N 910 -270 1050 -270 { lab=vss}
N 1090 -370 1110 -370 { lab=vss}
N 1110 -370 1110 -270 { lab=vss}
N 1090 -270 1110 -270 { lab=vss}
N 1090 -500 1090 -400 { lab=#net1}
N 890 -400 910 -400 { lab=vss}
N 910 -400 910 -370 { lab=vss}
N 430 -890 430 -860 { lab=vdd}
N 410 -860 430 -860 { lab=vdd}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 550 -640 0 0 {name=M1
L=1
W=48
nf=8
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 770 -640 0 0 {name=M2
L=1
W=48
nf=8
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 550 -890 0 0 {name=M3
L=1
W=24
nf=8
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 770 -890 0 0 {name=M4
L=1
W=24
nf=8
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/spice_probe.sym} 850 -760 0 0 {name=p2 attrs="" voltage=1.89}
C {devices/lab_pin.sym} 790 -800 0 1 {name=l2 sig_type=std_logic lab=vo}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 670 -370 0 0 {name=M5
L=1
W=96
nf=16
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 310 -370 0 0 {name=M6
L=1
W=12
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 390 -890 0 0 {name=M7
L=1
W=6
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 870 -370 0 0 {name=M8
L=1
W=12
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 1070 -370 0 0 {name=M9
L=1
W=12
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {devices/iopin.sym} 180 -980 2 0 {name=p15 lab=vdd}
C {devices/iopin.sym} 180 -270 2 0 {name=p16 lab=vss}
C {devices/ngspice_get_expr.sym} 790 -830 0 0 {name=r6 node="[expr 1/[ngspice::get_node \{@m.xm4.msky130_fd_pr__pfet_g5v0d10v5[gds]\}]]"
descr="rds4"}
C {devices/spice_probe.sym} 570 -750 0 0 {name=p1 attrs=""}
C {devices/spice_probe.sym} 490 -640 0 0 {name=p3 attrs=""}
C {devices/spice_probe.sym} 490 -580 0 0 {name=p4 attrs=""}
C {devices/spice_probe.sym} 670 -500 0 0 {name=p6 attrs=""}
C {devices/ngspice_get_expr.sym} 790 -710 0 0 {name=r7 node="[expr 1/( ([ngspice::get_node \{@m.xm4.msky130_fd_pr__pfet_g5v0d10v5[gds]\}]) + ([ngspice::get_node \{@m.xm2.msky130_fd_pr__nfet_g5v0d10v5[gds]\}]) ) ]"
descr="rds2||rds4"}
C {devices/ngspice_get_expr.sym} 890 -640 0 0 {name=r1 node="[expr [ngspice::get_node \{@m.xm2.msky130_fd_pr__nfet_g5v0d10v5[gm]\}]]"
descr="gm2"}
C {devices/ngspice_get_expr.sym} 890 -610 0 0 {name=r2 node="[expr 1/[ngspice::get_node \{@m.xm2.msky130_fd_pr__nfet_g5v0d10v5[gds]\}]]"
descr="rds2"}
C {devices/iopin.sym} 330 -480 3 0 {name=p5 lab=ir25u}
C {devices/iopin.sym} 470 -580 2 0 {name=p7 lab=vin}
C {devices/iopin.sym} 470 -640 2 0 {name=p8 lab=vip}
C {devices/iopin.sym} 890 -760 0 0 {name=p9 lab=vo}
C {common/border.sym} 0 0 0 0 {design_name="LDO Amp"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
