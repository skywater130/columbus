v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 730 -230 730 -160 { lab=vreg}
N 120 -280 400 -280 { lab=vref}
N 730 -430 730 -290 { lab=vdd}
N 730 -260 750 -260 { lab=vss}
N 730 -160 830 -160 { lab=vreg}
N 750 -260 750 -100 { lab=vss}
N 480 -100 750 -100 { lab=vss}
N 120 -430 730 -430 { lab=vdd}
N 120 -240 400 -240 { lab=vfb}
N 240 -140 240 -100 { lab=vss}
N 120 -100 240 -100 { lab=vss}
N 240 -320 240 -200 { lab=ir25u}
N 400 -280 450 -260 { lab=vref}
N 400 -240 450 -200 { lab=vfb}
N 480 -200 480 -100 { lab=vss}
N 240 -100 480 -100 { lab=vss}
N 480 -260 540 -260 { lab=#net1}
N 600 -260 690 -260 { lab=vg}
C {sky130_fd_pr/nfet_05v0_nvt.sym} 710 -260 0 0 {name=M1
L=0.9
W=340
nf=34
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_05v0_nvt
spiceprefix=X
}
C {devices/lab_pin.sym} 660 -260 1 0 {name=l3 sig_type=std_logic lab=vg}
C {devices/ipin.sym} 120 -280 0 0 {name=p1 lab=vref}
C {devices/ipin.sym} 240 -320 1 0 {name=p2 lab=ir25u}
C {devices/iopin.sym} 120 -430 2 0 {name=p3 lab=vdd}
C {devices/iopin.sym} 120 -100 2 0 {name=p4 lab=vss}
C {devices/opin.sym} 830 -160 0 0 {name=p5 lab=vreg}
C {devices/ipin.sym} 120 -240 0 0 {name=p6 lab=vfb}
C {res.sym} 240 -170 0 0 {
name=R1
value=1
}
C {vcvs.sym} 480 -230 0 0 {
name=E1
gain=1000
}
C {res.sym} 570 -260 1 0 {
name=R2
value=1k
}
