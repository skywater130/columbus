v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 210 -320 250 -320 { lab=vreg}
N 210 -320 210 -260 { lab=vreg}
N 210 -260 450 -260 { lab=vreg}
N 450 -340 450 -260 { lab=vreg}
N 350 -340 450 -340 { lab=vreg}
N 450 -340 510 -340 { lab=vreg}
N 150 -360 250 -360 { lab=vref}
N 150 -440 300 -440 { lab=vdd}
N 150 -220 300 -220 { lab=vss}
N 330 -430 330 -380 { lab=ir25u}
N 300 -440 300 -380 { lab=vdd}
N 300 -300 300 -220 { lab=vss}
C {devices/iopin.sym} 150 -440 2 0 {name=p3 lab=vdd}
C {devices/iopin.sym} 150 -220 2 0 {name=p4 lab=vss}
C {ldo/sch/ldo_base.sym} 300 -340 0 0 {
name=I1
}
C {devices/iopin.sym} 150 -360 2 0 {name=p6 lab=vref}
C {devices/iopin.sym} 330 -430 3 0 {name=p7 lab=ir25u}
C {devices/iopin.sym} 510 -340 0 0 {name=p8 lab=vreg}
C {common/border.sym} 0 0 0 0 {design_name="LDO Top-Level"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
