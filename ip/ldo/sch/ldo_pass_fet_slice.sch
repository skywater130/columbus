v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 320 -230 320 -160 { lab=vout}
N 320 -430 320 -290 { lab=vdd}
N 320 -260 340 -260 { lab=vss}
N 320 -160 420 -160 { lab=vout}
N 340 -260 340 -100 { lab=vss}
N 160 -130 160 -100 { lab=vss}
N 120 -100 160 -100 { lab=vss}
N 200 -130 220 -130 { lab=vss}
N 220 -130 220 -100 { lab=vss}
N 160 -100 220 -100 { lab=vss}
N 200 -430 200 -160 { lab=vdd}
N 120 -430 200 -430 { lab=vdd}
N 200 -430 320 -430 { lab=vdd}
N 220 -100 340 -100 { lab=vss}
N 120 -260 280 -260 { lab=vg}
C {sky130_fd_pr/nfet_05v0_nvt.sym} 300 -260 0 0 {name=M1
L=0.9
W=340
nf=34
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_05v0_nvt
spiceprefix=X
}
C {devices/ipin.sym} 120 -260 0 0 {name=p1 lab=vg}
C {devices/iopin.sym} 120 -430 2 0 {name=p3 lab=vdd}
C {devices/iopin.sym} 120 -100 2 0 {name=p4 lab=vss}
C {devices/opin.sym} 420 -160 0 0 {name=p5 lab=vout}
C {sky130_fd_pr/nfet_05v0_nvt.sym} 180 -130 0 0 {name=M2
L=0.9
W=20
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_05v0_nvt
spiceprefix=X
}
