v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 480 -420 690 -420 { lab=vg}
N 440 -590 440 -460 { lab=vdd}
N 730 -390 730 -320 { lab=vreg}
N 120 -440 400 -440 { lab=vref}
N 440 -260 750 -260 { lab=vss}
N 730 -590 730 -450 { lab=vdd}
N 440 -590 730 -590 { lab=vdd}
N 730 -420 750 -420 { lab=vss}
N 730 -320 830 -320 { lab=vreg}
N 440 -380 440 -260 { lab=vss}
N 750 -420 750 -260 { lab=vss}
N 410 -620 410 -460 { lab=ir25u}
N 200 -590 440 -590 { lab=vdd}
N 220 -260 440 -260 { lab=vss}
N 160 -290 160 -260 { lab=vss}
N 120 -260 160 -260 { lab=vss}
N 200 -290 220 -290 { lab=vss}
N 220 -290 220 -260 { lab=vss}
N 160 -260 220 -260 { lab=vss}
N 200 -590 200 -320 { lab=vdd}
N 120 -590 200 -590 { lab=vdd}
N 120 -400 400 -400 { lab=vfb}
C {sky130_fd_pr/nfet_05v0_nvt.sym} 710 -420 0 0 {name=M1
L=0.9
W=340
nf=34
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_05v0_nvt
spiceprefix=X
}
C {devices/lab_pin.sym} 610 -420 1 0 {name=l3 sig_type=std_logic lab=vg}
C {devices/iopin.sym} 120 -590 2 0 {name=p3 lab=vdd}
C {devices/iopin.sym} 120 -260 2 0 {name=p4 lab=vss}
C {sky130_fd_pr/nfet_05v0_nvt.sym} 180 -290 0 0 {name=M2
L=0.9
W=20
nf=2
mult=3
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_05v0_nvt
spiceprefix=X
}
C {ldo/sch/ldo_amp.sym} 440 -420 0 0 {
name=I1
}
C {devices/iopin.sym} 120 -440 2 0 {name=p7 lab=vref}
C {devices/iopin.sym} 120 -400 2 0 {name=p8 lab=vfb}
C {devices/iopin.sym} 410 -620 3 0 {name=p9 lab=ir25u}
C {devices/iopin.sym} 830 -320 0 0 {name=p10 lab=vreg}
C {common/border.sym} 0 0 0 0 {design_name="LDO Base"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
