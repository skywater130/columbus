v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 390 -380 390 -340 { lab=ldo_amp_ir25u}
N 260 -380 390 -380 { lab=ldo_amp_ir25u}
N 260 -320 380 -320 { lab=ldo_amp_vip}
N 260 -280 380 -280 { lab=ldo_amp_vin}
N 390 -600 390 -560 { lab=bgr_amp_ir50u}
N 260 -600 390 -600 { lab=bgr_amp_ir50u}
N 260 -540 380 -540 { lab=bgr_amp_vip}
N 260 -500 380 -500 { lab=bgr_amp_vin}
N 460 -560 500 -560 { lab=bgr_amp_vcomp}
N 460 -520 500 -520 { lab=bgr_amp_vo}
N 460 -300 500 -300 { lab=ldo_amp_vo}
N 450 -960 500 -960 { lab=bgr_ir20u}
N 450 -1000 450 -960 { lab=bgr_ir20u}
N 460 -1060 500 -1060 { lab=bgr_vref}
N 260 -880 440 -880 { lab=ldo_ir25u}
N 440 -880 440 -800 { lab=ldo_ir25u}
N 260 -780 360 -780 { lab=ldo_vref}
N 460 -760 500 -760 { lab=ldo_vreg}
C {common/border.sym} 0 0 0 0 {design_name="Top - Individual Blocks"
revision="A0"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {devices/iopin.sym} 260 -1240 2 0 {name=p1 lab=vdda1}
C {bgr/sch/bgr_top.sym} 420 -1060 0 0 {
name=I5
}
C {bgr/sch/bgr_amp.sym} 420 -520 0 0 {
name=I6
}
C {ldo/sch/ldo_amp.sym} 420 -300 0 0 {
name=I7
}
C {ldo/sch/ldo_top.sym} 410 -760 0 0 {
name=I8
}
C {devices/iopin.sym} 260 -380 2 0 {name=p9 lab=ldo_amp_ir25u}
C {devices/iopin.sym} 260 -320 2 0 {name=p10 lab=ldo_amp_vip}
C {devices/iopin.sym} 260 -280 2 0 {name=p11 lab=ldo_amp_vin}
C {devices/iopin.sym} 260 -600 2 0 {name=p6 lab=bgr_amp_ir50u}
C {devices/iopin.sym} 260 -540 2 0 {name=p12 lab=bgr_amp_vip}
C {devices/iopin.sym} 260 -500 2 0 {name=p13 lab=bgr_amp_vin}
C {devices/iopin.sym} 500 -560 2 1 {name=p14 lab=bgr_amp_vcomp}
C {devices/lab_pin.sym} 420 -340 1 0 {name=l1 sig_type=std_logic lab=vdda1}
C {devices/lab_pin.sym} 420 -560 1 0 {name=l2 sig_type=std_logic lab=vdda1}
C {devices/lab_pin.sym} 420 -480 3 0 {name=l3 sig_type=std_logic lab=vssa1}
C {devices/lab_pin.sym} 420 -260 3 0 {name=l4 sig_type=std_logic lab=vssa1}
C {devices/iopin.sym} 500 -520 2 1 {name=p15 lab=bgr_amp_vo}
C {devices/iopin.sym} 500 -300 2 1 {name=p16 lab=ldo_amp_vo}
C {devices/lab_pin.sym} 420 -1120 1 0 {name=l5 sig_type=std_logic lab=vdda1}
C {devices/lab_pin.sym} 420 -1000 3 0 {name=l6 sig_type=std_logic lab=vssa1}
C {devices/iopin.sym} 500 -960 2 1 {name=p17 lab=bgr_ir20u}
C {devices/iopin.sym} 500 -1060 2 1 {name=p18 lab=bgr_vref}
C {devices/lab_pin.sym} 410 -800 1 0 {name=l7 sig_type=std_logic lab=vdda1}
C {devices/lab_pin.sym} 410 -720 3 0 {name=l8 sig_type=std_logic lab=vssa1}
C {devices/iopin.sym} 260 -880 2 0 {name=p19 lab=ldo_ir25u}
C {devices/iopin.sym} 260 -780 2 0 {name=p20 lab=ldo_vref}
C {devices/iopin.sym} 500 -760 2 1 {name=p21 lab=ldo_vreg}
C {devices/iopin.sym} 260 -1220 2 0 {name=p28 lab=vssa1}
