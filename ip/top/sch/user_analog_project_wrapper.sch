v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 720 -920 720 -880 { lab=gpio_noesd[4]}
N 720 -880 820 -880 { lab=gpio_noesd[4]}
N 730 -980 820 -980 { lab=gpio_noesd[5]}
N 1070 -1330 1350 -1330 { lab=#net1}
N 1350 -1330 1350 -1200 { lab=#net1}
N 990 -1370 1150 -1370 { lab=io_analog[4]}
N 990 -1350 1150 -1350 { lab=io_analog[5]}
N 990 -1310 1150 -1310 { lab=gpio_noesd[7]}
N 990 -1290 1150 -1290 { lab=vssa2}
C {devices/iopin.sym} 250 -1350 0 0 {name=p1 lab=vdda1}
C {devices/iopin.sym} 250 -1320 0 0 {name=p2 lab=vdda2}
C {devices/iopin.sym} 250 -1290 0 0 {name=p3 lab=vssa1}
C {devices/iopin.sym} 250 -1260 0 0 {name=p4 lab=vssa2}
C {devices/iopin.sym} 250 -1230 0 0 {name=p5 lab=vccd1}
C {devices/iopin.sym} 250 -1200 0 0 {name=p6 lab=vccd2}
C {devices/iopin.sym} 250 -1170 0 0 {name=p7 lab=vssd1}
C {devices/iopin.sym} 250 -1140 0 0 {name=p8 lab=vssd2}
C {devices/ipin.sym} 300 -1070 0 0 {name=p9 lab=wb_clk_i}
C {devices/ipin.sym} 300 -1040 0 0 {name=p10 lab=wb_rst_i}
C {devices/ipin.sym} 300 -1010 0 0 {name=p11 lab=wbs_stb_i}
C {devices/ipin.sym} 300 -980 0 0 {name=p12 lab=wbs_cyc_i}
C {devices/ipin.sym} 300 -950 0 0 {name=p13 lab=wbs_we_i}
C {devices/ipin.sym} 300 -920 0 0 {name=p14 lab=wbs_sel_i[3:0]}
C {devices/ipin.sym} 300 -890 0 0 {name=p15 lab=wbs_dat_i[31:0]}
C {devices/ipin.sym} 300 -860 0 0 {name=p16 lab=wbs_adr_i[31:0]}
C {devices/opin.sym} 290 -800 0 0 {name=p17 lab=wbs_ack_o}
C {devices/opin.sym} 290 -770 0 0 {name=p18 lab=wbs_dat_o[31:0]}
C {devices/ipin.sym} 300 -730 0 0 {name=p19 lab=la_data_in[127:0]}
C {devices/opin.sym} 290 -700 0 0 {name=p20 lab=la_data_out[127:0]}
C {devices/ipin.sym} 300 -620 0 0 {name=p21 lab=io_in[26:0]}
C {devices/ipin.sym} 300 -590 0 0 {name=p22 lab=io_in_3v3[26:0]}
C {devices/ipin.sym} 290 -310 0 0 {name=p23 lab=user_clock2}
C {devices/opin.sym} 290 -560 0 0 {name=p24 lab=io_out[26:0]}
C {devices/opin.sym} 290 -530 0 0 {name=p25 lab=io_oeb[26:0]}
C {devices/iopin.sym} 260 -470 0 0 {name=p26 lab=gpio_analog[17:0]}
C {devices/iopin.sym} 260 -440 0 0 {name=p27 lab=gpio_noesd[17:0]}
C {devices/iopin.sym} 260 -410 0 0 {name=p29 lab=io_analog[10:0]}
C {devices/iopin.sym} 260 -380 0 0 {name=p30 lab=io_clamp_high[2:0]}
C {devices/iopin.sym} 260 -350 0 0 {name=p31 lab=io_clamp_low[2:0]}
C {devices/opin.sym} 280 -280 0 0 {name=p32 lab=user_irq[2:0]}
C {devices/ipin.sym} 300 -670 0 0 {name=p28 lab=la_oenb[127:0]}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {common/border.sym} 0 0 0 0 {design_name="design name"
revision="00"
author="author"
name="border"
}
C {devices/lab_pin.sym} 820 -980 2 0 {name=l2 sig_type=std_logic lab=gpio_noesd[5]}
C {devices/lab_pin.sym} 820 -880 2 0 {name=l1 sig_type=std_logic lab=gpio_noesd[4]}
C {devices/lab_pin.sym} 690 -1040 1 0 {name=l3 sig_type=std_logic lab=vdda1}
C {devices/lab_pin.sym} 690 -920 3 0 {name=l4 sig_type=std_logic lab=vssa1}
C {pswitch/sch/pswitch_top.sym} 1030 -1330 0 0 {
name=I2
}
C {ldo/sch/ldo_top.sym} 1350 -1160 0 0 {
name=I3
}
C {pswitch/sch/pswitch_top.sym} 1110 -1330 0 0 {
name=I4
}
C {pswitch/sch/pswitch_top.sym} 1190 -1330 0 0 {
name=I5
}
C {devices/lab_pin.sym} 990 -1350 0 0 {name=l6 sig_type=std_logic lab=io_analog[5]}
C {devices/lab_pin.sym} 990 -1370 0 0 {name=l7 sig_type=std_logic lab=io_analog[4]}
C {devices/lab_pin.sym} 990 -1310 0 0 {name=l8 sig_type=std_logic lab=gpio_noesd[7]}
C {devices/lab_pin.sym} 990 -1290 0 0 {name=l9 sig_type=std_logic lab=vssa2}
C {devices/lab_pin.sym} 1400 -1160 2 0 {name=l10 sig_type=std_logic lab=io_analog[3]}
C {devices/lab_pin.sym} 1300 -1180 0 0 {name=l11 sig_type=std_logic lab=gpio_noesd[8]}
C {devices/lab_pin.sym} 1380 -1200 1 0 {name=l12 sig_type=std_logic lab=gpio_noesd[9]}
C {devices/lab_pin.sym} 1350 -1120 3 0 {name=l5 sig_type=std_logic lab=vssa2}
C {bgr/sch/bgr_amp.sym} 1420 -800 0 0 {
name=I1
}
C {devices/lab_pin.sym} 1380 -820 0 0 {name=l13 sig_type=std_logic lab=io_analog[8]}
C {devices/lab_pin.sym} 1380 -780 0 0 {name=l14 sig_type=std_logic lab=io_analog[9]}
C {devices/lab_pin.sym} 1460 -800 2 0 {name=l15 sig_type=std_logic lab=io_analog[7]}
C {devices/lab_pin.sym} 1460 -840 2 0 {name=l16 sig_type=std_logic lab=io_analog[6]}
C {devices/lab_pin.sym} 1390 -840 1 0 {name=l17 sig_type=std_logic lab=io_analog[10]}
C {devices/lab_pin.sym} 1420 -760 3 0 {name=l18 sig_type=std_logic lab=vssa2}
C {devices/lab_pin.sym} 1420 -840 1 0 {name=l19 sig_type=std_logic lab=vdda2}
C {bgr/sch/bgr_top.sym} 690 -980 0 0 {
name=I6
}
