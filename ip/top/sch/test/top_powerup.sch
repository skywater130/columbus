v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 760 -460 760 -420 { lab=gpio_noesd[8]}
C {common/border.sym} 0 0 0 0 {design_name="Step enable test"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {common/spice.sym} 1100 -660 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.options savecurrents
.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

op
tran 100n 2m

plot vreg

.endc
"
}
C {vsource.sym} 60 -230 0 0 {
name=V1
value="dc 0 pwl(0 0 1m 3.3)"
}
C {devices/gnd.sym} 60 -180 0 0 {name=l1 lab=GND}
C {top/sch/user_analog_project_wrapper.sym} 350 -830 0 0 {name=x1}
C {synonym.sym} 60 -190 0 0 {
name=J1
}
C {devices/lab_pin.sym} 60 -200 0 0 {name=l3 sig_type=std_logic lab=vss}
C {devices/lab_pin.sym} 500 -950 2 0 {name=l4 sig_type=std_logic lab=vss}
C {devices/lab_pin.sym} 500 -970 2 0 {name=l5 sig_type=std_logic lab=vss}
C {devices/lab_pin.sym} 60 -260 1 0 {name=l6 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 500 -1010 2 0 {name=l7 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 500 -990 2 0 {name=l8 sig_type=std_logic lab=vdda}
C {res.sym} 760 -490 0 0 {
name=R2
value=1k
}
C {res.sym} 760 -390 0 0 {
name=R4
value=1k
}
C {devices/lab_pin.sym} 760 -360 3 0 {name=l9 sig_type=std_logic lab=vss}
C {devices/lab_pin.sym} 760 -520 1 0 {name=l10 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 760 -440 2 0 {name=l11 sig_type=std_logic lab=gpio_noesd[8]}
C {devices/lab_pin.sym} 500 -730 2 0 {name=l12 sig_type=std_logic lab=gpio_noesd[17:0]}
C {vccs.sym} 290 -230 0 0 {
name=G1
gain='1/3.3*25u'
}
C {devices/lab_pin.sym} 290 -260 1 0 {name=l13 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 260 -260 1 0 {name=l14 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 260 -200 3 0 {name=l15 sig_type=std_logic lab=vss}
C {devices/lab_pin.sym} 290 -200 2 0 {name=l16 sig_type=std_logic lab=gpio_noesd[9]}
C {devices/lab_pin.sym} 760 -300 2 0 {name=l18 sig_type=std_logic lab=gpio_noesd[7]}
C {synonym.sym} 750 -300 1 0 {
name=J3
}
C {devices/lab_pin.sym} 740 -300 0 0 {name=l20 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 500 -710 2 0 {name=l17 sig_type=std_logic lab=io_analog[10:0]}
C {devices/lab_pin.sym} 760 -240 2 0 {name=l19 sig_type=std_logic lab=io_analog[5]}
C {devices/lab_pin.sym} 760 -260 2 0 {name=l21 sig_type=std_logic lab=io_analog[4]}
C {synonym.sym} 750 -260 1 0 {
name=J2
}
C {devices/lab_pin.sym} 740 -260 0 0 {name=l22 sig_type=std_logic lab=vdda}
C {synonym.sym} 750 -240 1 0 {
name=J4
}
C {devices/lab_pin.sym} 740 -240 0 0 {name=l23 sig_type=std_logic lab=vdda}
C {devices/lab_pin.sym} 740 -200 0 0 {name=l24 sig_type=std_logic lab=io_analog[3]}
C {synonym.sym} 750 -200 1 0 {
name=J5
}
C {devices/lab_pin.sym} 760 -200 2 0 {name=l25 sig_type=std_logic lab=vreg}
