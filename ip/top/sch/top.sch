v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 820 -530 820 -400 { lab=#net1}
N 150 -570 370 -570 { lab=vddp}
N 150 -550 370 -550 { lab=vddc}
N 820 -320 820 -300 { lab=vss}
N 180 -300 820 -300 { lab=vss}
N 150 -380 770 -380 { lab=vref}
N 290 -530 820 -530 { lab=#net1}
N 180 -490 370 -490 { lab=vss}
N 150 -510 370 -510 { lab=en}
N 870 -360 960 -360 { lab=vreg}
N 850 -570 850 -400 { lab=ir25u}
N 180 -490 180 -300 { lab=vss}
N 150 -300 180 -300 { lab=vss}
C {common/border.sym} 0 0 0 0 {design_name="Component Test 1"
revision="A0"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {ldo/sch/ldo_top.sym} 820 -360 0 0 {
name=I1
}
C {pswitch/sch/pswitch_top.sym} 250 -530 0 0 {
name=I2
}
C {pswitch/sch/pswitch_top.sym} 330 -530 0 0 {
name=I3
}
C {pswitch/sch/pswitch_top.sym} 410 -530 0 0 {
name=I4
}
C {devices/iopin.sym} 150 -570 2 0 {name=p1 lab=vddp}
C {devices/iopin.sym} 150 -550 2 0 {name=p2 lab=vddc}
C {devices/iopin.sym} 150 -300 2 0 {name=p3 lab=vss}
C {devices/iopin.sym} 150 -510 2 0 {name=p4 lab=en}
C {devices/iopin.sym} 150 -380 2 0 {name=p5 lab=vref}
C {devices/iopin.sym} 850 -570 3 0 {name=p7 lab=ir25u}
C {devices/iopin.sym} 960 -360 0 0 {name=p8 lab=vreg}
