v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 160 -260 160 -220 { lab=#net1}
N 160 -220 340 -220 { lab=#net1}
N 340 -260 340 -220 { lab=#net1}
N 380 -260 380 -220 { lab=#net2}
N 380 -220 560 -220 { lab=#net2}
N 560 -260 560 -220 { lab=#net2}
N 600 -260 600 -220 { lab=#net3}
N 600 -220 780 -220 { lab=#net3}
N 780 -260 780 -220 { lab=#net3}
N 820 -260 820 -220 { lab=#net4}
N 820 -220 1000 -220 { lab=#net4}
N 1000 -260 1000 -220 { lab=#net4}
N 340 -280 380 -280 { lab=#net5}
N 560 -280 600 -280 { lab=#net6}
N 780 -280 820 -280 { lab=#net7}
N 1000 -280 1040 -280 { lab=clk_div16}
N 120 -280 160 -280 { lab=clk}
C {common/border.sym} 0 0 0 0 {design_name="Clock Divider"
revision="00"
author="Tom"
name="border"
}
C {sky130_stdcells/dfxbp_1.sym} 250 -270 0 0 {name=x1 VGND=vss VNB=vss VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {devices/iopin.sym} 120 -280 2 0 {name=p1 lab=clk}
C {devices/iopin.sym} 1040 -280 0 0 {name=p2 lab=clk_div16}
C {devices/iopin.sym} 120 -380 2 0 {name=p3 lab=vdd}
C {devices/iopin.sym} 120 -340 2 0 {name=p4 lab=vss}
C {sky130_stdcells/dfxbp_1.sym} 470 -270 0 0 {name=x2 VGND=vss VNB=vss VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/dfxbp_1.sym} 690 -270 0 0 {name=x3 VGND=vss VNB=vss VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {sky130_stdcells/dfxbp_1.sym} 910 -270 0 0 {name=x4 VGND=vss VNB=vss VPB=vdd VPWR=vdd prefix=sky130_fd_sc_hd__ }
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
