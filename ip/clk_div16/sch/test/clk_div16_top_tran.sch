v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 160 -440 160 -400 { lab=GND}
N 280 -400 540 -400 { lab=GND}
N 540 -480 540 -400 { lab=GND}
N 540 -600 540 -560 { lab=#net1}
N 160 -600 540 -600 { lab=#net1}
N 160 -600 160 -500 { lab=#net1}
N 280 -440 280 -400 { lab=GND}
N 160 -400 280 -400 { lab=GND}
N 280 -520 280 -500 { lab=clk}
N 280 -520 500 -520 { lab=clk}
N 580 -520 660 -520 { lab=clk_out}
C {common/border.sym} 0 0 0 0 {design_name="CLK divide by 16 tran test"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {clk_div16/sch/clk_div16_top.sym} 540 -520 0 0 {
name=I1
}
C {vsource.sym} 160 -470 0 0 {
name=V1
value="dc 1.8"
}
C {devices/gnd.sym} 160 -400 0 0 {name=l2 lab=GND}
C {vsource.sym} 280 -470 0 0 {
name=V2
value="dc 0 pulse(0 1.8 500n 0 0 500n 1u)"
}
C {devices/lab_pin.sym} 660 -520 0 1 {name=l3 sig_type=std_logic lab=clk_out}
C {common/spice.sym} 840 -600 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.options savecurrents
.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

tran 100n 100u

plot clk clk_out

.endc
"
}
C {devices/lab_pin.sym} 280 -520 0 0 {name=l1 sig_type=std_logic lab=clk}
