# Bandgap Reference (1.211V-1.213V for -30C to 85C)

## Top-Level
![alt text](img/lay_top.png "Bandgap reference top-level layout")

The top-level circuit diagram showing a PTAT on the left, and a CTAT on the right is shown below.  The PTAT delta V<sub>BE</sub> scale factor is chosen to match the CTAT temperature gradient at ~300K.  See simulation results in the pages which follow.  A single resistor R3 serves as a startup circuit for the bandgap by requiring the high-side FETs to source a non-zero current.  Since the amplifier is biased by the same, a zero current condition results in the amplifier output being high impedance thus providing no contention to the resistor to ground.  As soon as current starts to flow, the amplifier starts up providing active feedback with negligible contention from R3. C1 and R4 serve as the compensation network required to ensure loop stability as will be discussed later.

![alt text](img/sch_top.png "Bandgap reference top-level schematic")

Below is the BJT array captured in the instances I1, I2, and I4 above.

![alt text](img/bjt_array.png "BJT array")

## Amplifier
The bandgap amplifier schematic is shown below along with its uncompensated AC response.  The amplifier is a folded-cascode two stage opamp with almost 80dB of gain at DC.  A PMOS input stage was chosen so as to maximise the low-end common-mode voltage range required by the BJTs, and whilst this also gives the advantage of being able to tie the input pair’s bulk to source, the fact that this is a deep-nwell process would also have allowed for the same if an NMOS input stage were used instead.  A folded-cascode topology was chosen so as to maximize the input impedance of the amplifier.  A wide swing cascode current mirror bias network was chosen to provide the tail current consisting of L=1um PFETs over the originally chosen L=5um non-cascode network to improve the layout of the amplifier.

![alt text](img/sch_amp.png "Bandgap amplifier schematic")

![alt text](img/ac_amp.png "Bandgap amplifier ac response")

## Stability Analysis & Compensation
The amplifier was designed to be dominant pole compensated with the external RC compensation network shown earlier in the top-level schematic.  The stability analysis testbench schematic is shown below, it breaks the loop at the amplifier output, loading it with a copy of the load it would see under normal operation, and injects the test signal at the shared gate net of the PMOS current sources.  The DC offset is set to the operating point of the closed-loop configuration at 27C.

![alt text](img/sch_bgr_stab.png "Stability analysis testbench schematic")

The frequency response of the un-compensated loop is shown below, and has approximately -15 degrees of phase margin.

![alt text](img/ac_bgr1.png "Uncompensated loop transfer function")

The frequency response of the compensated loop is shown below, where the calculated compensation component values are C<sub>C</sub>=994f, and R<sub>C</sub>=11.38k.  PM is 81 degrees, GM is 10.3dB, and ft is 9.8MHz.

![alt text](img/ac_bgr2.png "Compensated loop transfer function")

Since the loop’s feedback function beta is both non-linear and non-monotonic, a Nyquist plot was generated to check loop stability using the on-axis circle criterion.

![alt text](img/nyquist_bgr1.png "Nyquist plot of the loop")

The vertical blue line in the above Nyquist plot crosses the real line at +1 and is used only as an indication of scale since the axis markings are inaccurate for some reason (possibly a bug in the viewer?).  The below Nyquist plot zooms into the relevant portion of the above plot.  The blue line here is tangent to the point of maximum real value and is used to illustrate the perimeter of a circle of infinite diameter whose center is coincident with the real line.

![alt text](img/nyquist_bgr2.png "Region of interest of the Nyquist plot")

The red line on the plot below shows the output to input relationship of the feedback function beta illustrating it’s non-affine and non-monotonic behaviour.  It is clear that pulling an equivalent unity-gain saturating nonlinearity stage out of the amplifier and pushing it round the loop intowould only result in further compression of the red curve on the y-axis.  Given this and the fact that the red curve is already far from crossing the blue line derived from the circle criterion in the above Nyquist plot, we can say that the system is unconditionally stable despite the non-monotonic non-linearity.

![alt text](img/dc_beta.png "Output to input relationship of beta")

## Transient Analysis
The below schematic shows the transient test circuit which instantiates the bandgap reference circuit and ramps the power supply from 0V to 3.3V and back down to 0V over 10ms.

![alt text](img/tran_vdd_bgr.png "Supply Ramp Transient Test")

A temperature sweep at the DC operating point shows a temperature delta of approximately 3mV between -30C and 85C with a turning point at ~300K as desired.

![alt text](img/dc_temp_bgr.png "DC Temperature Sweep -30C to 85C")
