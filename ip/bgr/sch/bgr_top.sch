v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 220 -370 240 -370 { lab=#net1}
N 240 -370 240 -350 { lab=#net1}
N 220 -350 240 -350 { lab=#net1}
N 220 -320 260 -320 { lab=vref}
N 180 -400 180 -380 { lab=vdd}
N 180 -260 180 -240 { lab=vss}
N 210 -260 210 -240 { lab=ir20u}
C {common/border.sym} 0 0 0 0 {design_name="Bandgep Ref."
revision="00"
author="Tom"
name="border"
}
C {bgr/sch/bgr_ptat_ctat.sym} 180 -320 0 0 {
name=I1
}
C {devices/iopin.sym} 180 -400 3 0 {name=p1 lab=vdd}
C {devices/iopin.sym} 180 -240 1 0 {name=p2 lab=vss}
C {devices/iopin.sym} 260 -320 0 0 {name=p4 lab=vref}
C {devices/iopin.sym} 210 -240 1 0 {name=p5 lab=ir20u}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.include \\"\\\\$::ANALOG_LIB\\\\\\"
.include \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
