v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 800 -300 800 -250 { lab=gnd}
N 840 -250 900 -250 { lab=gnd}
N 900 -300 900 -250 { lab=gnd}
N 900 -250 940 -250 { lab=gnd}
N 840 -350 840 -330 { lab=ve}
N 840 -350 940 -350 { lab=ve}
N 940 -350 940 -330 { lab=ve}
N 700 -300 700 -250 { lab=gnd}
N 740 -250 800 -250 { lab=gnd}
N 740 -350 740 -330 { lab=ve}
N 740 -350 840 -350 { lab=ve}
N 600 -300 600 -250 { lab=gnd}
N 640 -250 700 -250 { lab=gnd}
N 640 -350 640 -330 { lab=ve}
N 640 -350 740 -350 { lab=ve}
N 500 -300 500 -250 { lab=gnd}
N 540 -250 600 -250 { lab=gnd}
N 540 -350 540 -330 { lab=ve}
N 540 -350 640 -350 { lab=ve}
N 400 -300 400 -250 { lab=gnd}
N 440 -250 500 -250 { lab=gnd}
N 440 -350 440 -330 { lab=ve}
N 440 -350 540 -350 { lab=ve}
N 300 -300 300 -250 { lab=gnd}
N 340 -250 400 -250 { lab=gnd}
N 340 -350 340 -330 { lab=ve}
N 340 -350 440 -350 { lab=ve}
N 200 -300 200 -250 { lab=gnd}
N 240 -250 300 -250 { lab=gnd}
N 240 -350 240 -330 { lab=ve}
N 240 -350 340 -350 { lab=ve}
N 160 -350 240 -350 { lab=ve}
N 160 -250 200 -250 { lab=gnd}
N 240 -270 240 -250 {}
N 200 -250 240 -250 { lab=gnd}
N 340 -270 340 -250 {}
N 300 -250 340 -250 { lab=gnd}
N 440 -270 440 -250 {}
N 400 -250 440 -250 { lab=gnd}
N 540 -270 540 -250 {}
N 500 -250 540 -250 { lab=gnd}
N 640 -270 640 -250 {}
N 600 -250 640 -250 { lab=gnd}
N 740 -270 740 -250 {}
N 700 -250 740 -250 { lab=gnd}
N 840 -270 840 -250 {}
N 800 -250 840 -250 { lab=gnd}
N 940 -270 940 -250 {}
C {sky130_fd_pr/pnp_05v5.sym} 220 -300 0 0 {name=Q1
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {devices/iopin.sym} 160 -350 2 0 {name=p1 lab=ve}
C {devices/iopin.sym} 160 -250 2 0 {name=p2 lab=gnd}
C {common/border.sym} 0 0 0 0 {design_name="PNP x8 Array"
revision="00"
author="Tom"
name="border"
}
C {sky130_fd_pr/pnp_05v5.sym} 320 -300 0 0 {name=Q2
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {sky130_fd_pr/pnp_05v5.sym} 420 -300 0 0 {name=Q3
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {sky130_fd_pr/pnp_05v5.sym} 520 -300 0 0 {name=Q4
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {sky130_fd_pr/pnp_05v5.sym} 620 -300 0 0 {name=Q5
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {sky130_fd_pr/pnp_05v5.sym} 720 -300 0 0 {name=Q6
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {sky130_fd_pr/pnp_05v5.sym} 820 -300 0 0 {name=Q7
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {sky130_fd_pr/pnp_05v5.sym} 920 -300 0 0 {name=Q8
model=pnp_05v5_W3p40L3p40
spiceprefix=X
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
