v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
P 4 5 470 -460 1050 -460 1050 -360 470 -360 470 -460 { dash=5}
P 4 5 300 -320 940 -320 940 -220 300 -220 300 -320 { dash=5}
P 4 5 310 -310 570 -310 570 -230 310 -230 310 -310 { dash=5}
T {14*469} 580 -450 0 0 0.2 0.2 {}
T {11*469} 940 -450 0 0 0.2 0.2 {}
T {Match} 470 -480 0 0 0.3 0.3 {}
T {Match} 300 -340 0 0 0.3 0.3 {}
T {100uA} 310 -1030 0 0 0.4 0.4 {}
T {10uA} 510 -1030 0 0 0.4 0.4 {}
T {50uA} 670 -1030 0 0 0.4 0.4 {}
T {100uA} 870 -1030 0 0 0.4 0.4 {}
T {20uA} 1030 -1030 0 0 0.4 0.4 {}
T {994fF*1.5} 550 -720 0 0 0.4 0.4 {}
T {11k38R*2/5} 550 -690 0 0 0.4 0.4 {}
N 340 -1000 340 -980 { lab=vdd}
N 360 -1000 540 -1000 { lab=vdd}
N 540 -1000 540 -980 { lab=vdd}
N 540 -950 560 -950 { lab=vdd}
N 560 -1000 560 -950 { lab=vdd}
N 540 -1000 560 -1000 { lab=vdd}
N 540 -380 540 -300 { lab=ve2}
N 540 -920 540 -500 { lab=vp}
N 340 -920 340 -500 { lab=vn}
N 900 -240 900 -200 { lab=vss}
N 860 -950 860 -900 { lab=vbiasp}
N 900 -950 920 -950 { lab=vdd}
N 920 -1000 920 -950 { lab=vdd}
N 900 -1000 920 -1000 { lab=vdd}
N 900 -1000 900 -980 { lab=vdd}
N 900 -380 900 -300 { lab=ve4}
N 900 -680 900 -440 { lab=vref}
N 340 -240 340 -200 { lab=vss}
N 540 -240 540 -200 { lab=vss}
N 340 -950 360 -950 { lab=vdd}
N 360 -1000 360 -950 { lab=vdd}
N 340 -1000 360 -1000 { lab=vdd}
N 300 -950 300 -900 { lab=vbiasp}
N 500 -950 500 -900 { lab=vbiasp}
N 540 -500 540 -440 { lab=vp}
N 420 -520 420 -500 { lab=vn}
N 340 -500 420 -500 { lab=vn}
N 460 -520 460 -500 { lab=vp}
N 460 -500 540 -500 { lab=vp}
N 340 -500 340 -300 { lab=vn}
N 340 -200 540 -200 { lab=vss}
N 700 -1000 700 -980 { lab=vdd}
N 660 -950 660 -900 { lab=vbiasp}
N 700 -950 720 -950 { lab=vdd}
N 720 -1000 720 -950 { lab=vdd}
N 700 -1000 720 -1000 { lab=vdd}
N 540 -200 900 -200 { lab=vss}
N 560 -1000 700 -1000 { lab=vdd}
N 500 -900 660 -900 { lab=vbiasp}
N 700 -860 700 -530 { lab=#net1}
N 480 -530 700 -530 { lab=#net1}
N 160 -380 160 -200 { lab=vss}
N 160 -200 340 -200 { lab=vss}
N 120 -200 160 -200 { lab=vss}
N 160 -900 300 -900 { lab=vbiasp}
N 160 -900 160 -440 { lab=vbiasp}
N 400 -900 500 -900 { lab=vbiasp}
N 400 -900 400 -880 { lab=vbiasp}
N 300 -900 400 -900 { lab=vbiasp}
N 1060 -1000 1060 -980 { lab=vdd}
N 920 -1000 1060 -1000 { lab=vdd}
N 1060 -1000 1080 -1000 { lab=vdd}
N 1080 -1000 1080 -950 { lab=vdd}
N 1060 -950 1080 -950 { lab=vdd}
N 1020 -950 1020 -900 { lab=vbiasp}
N 860 -900 1020 -900 { lab=vbiasp}
N 1060 -920 1060 -880 { lab=ir20u}
N 440 -780 440 -600 { lab=vo}
N 480 -780 480 -760 { lab=vo}
N 900 -680 980 -680 { lab=vref}
N 660 -900 860 -900 { lab=vbiasp}
N 280 -1000 340 -1000 { lab=vdd}
N 900 -920 900 -680 { lab=vref}
N 480 -620 480 -600 { lab=#net2}
N 440 -780 480 -780 { lab=vo}
N 440 -840 440 -780 { lab=vo}
N 250 -1000 280 -1000 { lab=vdd}
N 250 -1120 250 -1000 { lab=vdd}
N 220 -1000 250 -1000 { lab=vdd}
N 220 -1120 220 -1000 { lab=vdd}
N 120 -1000 220 -1000 { lab=vdd}
N 250 -1160 280 -1160 { lab=vdd}
N 280 -1160 280 -1000 { lab=vdd}
N 770 -1000 900 -1000 { lab=vdd}
N 770 -1120 770 -1000 { lab=vdd}
N 740 -1000 770 -1000 { lab=vdd}
N 700 -860 800 -860 { lab=#net1}
N 700 -920 700 -860 { lab=#net1}
N 740 -1160 770 -1160 { lab=vdd}
N 740 -1160 740 -1000 { lab=vdd}
N 800 -1120 800 -860 { lab=#net1}
N 720 -1000 740 -1000 { lab=vdd}
N 480 -700 480 -680 { lab=#net3}
N 720 -620 740 -620 { lab=vss}
N 720 -680 720 -620 { lab=vss}
N 720 -680 740 -680 { lab=vss}
C {devices/launcher.sym} 160 -1360 0 0 {name=h2
descr=Backannotate
tclcommand="ngspice::annotate"}
C {devices/lab_pin.sym} 540 -350 2 0 {name=l5 sig_type=std_logic lab=ve2}
C {common/border.sym} 0 0 0 0 {design_name="PTAT/CTAT"
revision="00"
author="Tom"
name="border"
}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 320 -950 0 0 {name=M2
L=5
W=360
nf=20
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 520 -950 0 0 {name=M1
L=5
W=36
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/lab_pin.sym} 540 -500 2 0 {name=l9 sig_type=std_logic lab=vp}
C {devices/lab_pin.sym} 340 -500 0 0 {name=l10 sig_type=std_logic lab=vn}
C {bgr/sch/bgr_pnp8x.sym} 900 -270 0 0 {
name=I4
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 880 -950 0 0 {name=M3
L=5
W=360
nf=20
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/lab_pin.sym} 900 -350 0 1 {name=l4 sig_type=std_logic lab=ve4}
C {sky130_fd_pr/res_high_po_1p41.sym} 540 -410 0 0 {name=R1
W=5.73
L='14*1.25*5.73'
model=res_high_po_5p73
spiceprefix=X
mult=1}
C {sky130_fd_pr/res_high_po_1p41.sym} 900 -410 0 0 {name=R2
W=5.73
L='11*1.25*5.73'
model=res_high_po_5p73
spiceprefix=X
mult=1}
C {bgr/sch/bgr_pnp8x.sym} 340 -270 0 0 {
name=I1
}
C {bgr/sch/bgr_pnp8x.sym} 540 -270 0 0 {
name=I2
}
C {devices/lab_pin.sym} 520 -410 0 0 {name=l8 sig_type=std_logic lab=vss}
C {devices/lab_pin.sym} 880 -410 0 0 {name=l11 sig_type=std_logic lab=vss}
C {devices/iopin.sym} 120 -1000 2 0 {name=p7 lab=vdd}
C {devices/iopin.sym} 120 -200 2 0 {name=p8 lab=vss}
C {devices/lab_pin.sym} 480 -560 2 0 {name=l13 sig_type=std_logic lab=vdd}
C {devices/lab_pin.sym} 400 -560 0 0 {name=l14 sig_type=std_logic lab=vss}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 680 -950 0 0 {name=M6
L=5
W=180
nf=10
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/lab_pin.sym} 140 -410 0 0 {name=l6 sig_type=std_logic lab=vss}
C {devices/iopin.sym} 980 -680 0 0 {name=p10 lab=vref}
C {devices/iopin.sym} 400 -880 1 0 {name=p11 lab=vbiasp}
C {devices/iopin.sym} 440 -840 3 0 {name=p1 lab=vo}
C {devices/ngspice_probe.sym} 340 -580 0 0 {name=r2}
C {devices/ngspice_probe.sym} 540 -580 0 0 {name=r3}
C {devices/ngspice_probe.sym} 700 -580 0 0 {name=r4}
C {devices/ngspice_probe.sym} 540 -320 0 0 {name=r6}
C {devices/ngspice_probe.sym} 900 -320 0 0 {name=r7}
C {devices/ngspice_get_value.sym} 160 -440 0 0 {name=r8 node=i(@b.$\{path\}xr3.xsky130_fd_pr__res_xhigh_po_5p73.brend[i])
descr="I="}
C {devices/ngspice_get_value.sym} 540 -460 0 0 {name=r9 node=i(@b.$\{path\}xr1.brend[i])
descr="I="}
C {devices/ngspice_get_value.sym} 900 -460 0 0 {name=r10 node=i(@b.$\{path\}xr2.brend[i])
descr="I="}
C {bgr/sch/bgr_amp.sym} 440 -560 1 1 {
name=I3
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 1040 -950 0 0 {name=M4
L=5
W=72
nf=4
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/iopin.sym} 1060 -880 1 0 {name=p2 lab=ir20u}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 770 -1140 3 1 {name=M7
L=5
W=36
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 250 -1140 1 0 {name=M5
L=5
W=36
nf=2
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/res_high_po_1p41.sym} 480 -650 0 0 {name=R5
W=5.73
L='2*40'
model=res_high_po_5p73
spiceprefix=X
mult=1}
C {sky130_fd_pr/cap_mim_m3_1.sym} 480 -730 0 0 {name=C2 model=cap_mim_m3_1 W=5 L=9.5 MF=15 spiceprefix=X}
C {devices/lab_pin.sym} 460 -650 3 0 {name=l1 sig_type=std_logic lab=vss}
C {sky130_fd_pr/res_high_po_1p41.sym} 160 -410 0 0 {name=R4
W=5.73
L='4*71.625'
model=res_high_po_5p73
spiceprefix=X
mult=1}
C {sky130_fd_pr/res_high_po_1p41.sym} 740 -650 0 0 {name=R3
W=5.73
L='40'
model=res_high_po_5p73
spiceprefix=X
mult=3}
C {devices/lab_pin.sym} 740 -620 3 0 {name=l2 sig_type=std_logic lab=vss}
