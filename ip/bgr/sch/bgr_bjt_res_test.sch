v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 80 -500 80 -320 { lab=vp}
N 80 -740 200 -740 { lab=GND}
N 80 -260 80 -220 { lab=GND}
N 200 -260 200 -220 { lab=GND}
N 80 -220 200 -220 { lab=GND}
N 80 -740 80 -700 { lab=GND}
N 20 -740 80 -740 { lab=GND}
N 200 -740 200 -700 { lab=GND}
N 200 -500 200 -320 { lab=vn}
N 80 -640 80 -560 { lab=#net1}
N 200 -640 200 -560 { lab=#net2}
N 280 -220 300 -220 { lab=GND}
N 300 -300 300 -220 { lab=GND}
N 280 -330 280 -220 { lab=GND}
N 300 -420 300 -360 { lab=vr1}
N 300 -740 300 -480 { lab=GND}
N 460 -220 480 -220 { lab=GND}
N 480 -300 480 -220 { lab=GND}
N 460 -330 460 -220 { lab=GND}
N 480 -420 480 -360 { lab=vr2}
N 480 -740 480 -480 { lab=GND}
N 300 -220 460 -220 { lab=GND}
N 200 -220 280 -220 { lab=GND}
N 300 -740 480 -740 { lab=GND}
N 200 -740 300 -740 { lab=GND}
C {devices/launcher.sym} 820 -960 0 0 {name=h2
descr=Backannotate
tclcommand="ngspice::annotate"}
C {devices/gnd.sym} 80 -220 0 0 {name=l8 lab=GND}
C {devices/lab_pin.sym} 80 -380 2 0 {name=l4 sig_type=std_logic lab=vp}
C {devices/lab_pin.sym} 200 -380 2 0 {name=l5 sig_type=std_logic lab=vn}
C {common/spice.sym} 760 -840 0 0 {name=Analysis
tclcommand="xschem netlist; xschem simulate"
only_toplevel=true
value="
.param mc_mm_switch=0

.param iref=100u

.csparam ir=\{iref\}
.control

save all

*dc srcnam vstart vstop vincr [src2 start2 stop2 incr2]
*ac dec nd fstart fstop
*tran tstep tstop <tstart <tmax>> <uic>

*dc temp 0 85 1
*plot \{vp1-vn1\} vp1 vn1
*measure dc vp1 deriv k1 at=27
*print k1

set temp=17
op

let vp=vp
let vn=vn
let vd=vp-vn

set temp=37
op

let vp=vp
let vn=vn
let vd=vp-vn

let kp=(op2.vp-op1.vp)/20
let kn=(op2.vn-op1.vn)/20
let kd=(op2.vd-op1.vd)/20

print kp kn kd

set temp=27
op
let res1=vr1/ir
let res2=vr2/ir
print res1 res2

.endc
"
}
C {common/border.sym} 0 0 0 0 {design_name="BJT/Res Test"
revision="00"
author="Tom"
name="border"
}
C {isource.sym} 80 -670 0 0 {
name=I3
value="dc \{iref\}"
}
C {devices/gnd.sym} 20 -740 0 0 {name=l2 lab=GND}
C {common/sky130libs.sym} 620 -30 0 0 {name=SKY130
only_toplevel=true
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
.option wnflag=1
"
}
C {bgr/sch/bgr_pnp8x.sym} 80 -290 0 0 {
name=I1
}
C {bgr/sch/bgr_pnp8x.sym} 200 -290 0 0 {
name=I2
}
C {isource.sym} 200 -670 0 0 {
name=I6
value="dc \{iref/10\}"
}
C {devices/ammeter.sym} 80 -530 0 0 {name=Vmeas}
C {devices/ammeter.sym} 200 -530 0 0 {name=Vmeas1}
C {devices/spice_probe.sym} 80 -440 0 0 {name=p1 attrs=""}
C {devices/spice_probe.sym} 200 -440 0 0 {name=p2 attrs=""}
C {sky130_fd_pr/res_high_po_1p41.sym} 300 -330 0 0 {name=R1
W=5.73
L='1.25*5.73'
model=res_high_po_5p73
spiceprefix=X
mult=1}
C {isource.sym} 300 -450 0 0 {
name=I4
value="dc \{iref\}"
}
C {devices/lab_pin.sym} 300 -390 2 0 {name=l1 sig_type=std_logic lab=vr1}
C {isource.sym} 480 -450 0 0 {
name=I5
value="dc \{iref\}"
}
C {devices/lab_pin.sym} 480 -390 2 0 {name=l3 sig_type=std_logic lab=vr2}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 480 -330 0 0 {name=R2
W=5.73
L='50*5.73'
model=res_xhigh_po_5p73
spiceprefix=X
mult=1}
