v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 240 -290 260 -290 { lab=0}
N 260 -290 260 -260 { lab=0}
N 200 -230 200 -200 { lab=#net1}
N 240 -230 260 -230 { lab=0}
N 260 -260 260 -230 { lab=0}
N 20 -260 260 -260 { lab=0}
N 200 -320 200 -290 { lab=#net2}
N 130 -320 200 -320 { lab=#net2}
N 130 -200 200 -200 { lab=#net1}
N 240 -320 380 -320 { lab=#net3}
N 240 -200 380 -200 { lab=#net4}
N 260 -260 380 -260 { lab=0}
C {devices/gnd.sym} 20 -260 0 0 {name=l4 lab=0}
C {devices/launcher.sym} 80 -640 0 0 {name=h2
descr="Backannotate"
tclcommand="ngspice::annotate"}
C {devices/code_shown.sym} 850 -920 0 0 {name=analysis
only_toplevel=false
value="

.option wnflag=1

.param length=5

.control

set filetype=ascii

save @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[gm]
+    @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[id]
+    @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[gds]
+    @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[vth]
+    @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[cgg]
+    @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[w]
+    @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[l]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[gm]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[id]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[gds]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[vth]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[cgg]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[w]
+    @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[l]

*foreach length 3u
*alter @m.xm1.msky130_fd_pr__nfet_g5v0d10v5[l] = $length
*alter @m.xm2.msky130_fd_pr__pfet_g5v0d10v5[l] = $length

dc vg 0 1.65 100m

set appendwrite
write gmoverid.raw

*write gmoverid.raw
*set appendwrite
*end

.endc
"
}
C {devices/code.sym} -130 -130 0 0 {name=libs
only_toplevel=false
format="tcleval( @value )"
value="
.inc \\"\\\\$::ANALOG_LIB\\\\\\"
.inc \\"\\\\$::PDK_FD_SC_HD\\\\\\"
.lib \\"\\\\$::PDK_FD_PR\\\\ tt\\"
"}
C {devices/launcher.sym} 80 -680 0 0 {name=h1
descr="N&S"
tclcommand="xschem netlist; xschem simulate"}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 220 -230 0 0 {name=M2
L=length
W=6
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_g5v0d10v5
spiceprefix=X
}
C {vsource.sym} 20 -290 0 0 {
name=Vg
value="dc 1.1"
}
C {sky130_fd_pr/nfet_g5v0d10v5.sym} 220 -290 0 0 {name=M1
L=length
W=6
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_g5v0d10v5
spiceprefix=X
}
C {vsource.sym} 380 -290 0 0 {
name=V1
value="dc \{3.3/2\}"
}
C {vcvs.sym} 130 -230 0 0 {
name=E1
gain=-1
}
C {devices/lab_pin.sym} 20 -320 1 0 {name=l1 sig_type=std_logic lab=vc}
C {devices/lab_pin.sym} 100 -200 3 0 {name=l2 sig_type=std_logic lab=vc}
C {vcvs.sym} 130 -290 0 0 {
name=E2
gain=1
}
C {devices/lab_pin.sym} 100 -320 1 0 {name=l3 sig_type=std_logic lab=vc}
C {vsource.sym} 380 -230 0 0 {
name=V2
value="dc \{3.3/2\}"
}
