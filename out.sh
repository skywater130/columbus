#!/bin/bash

if [ -z ${PROJECT_ROOT+x} ]; then
    echo "please source \"envsetup.sh\""
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "USAGE:"
    echo "    \$out <block-name>"
    echo ""
    echo "    This will flatten '<block-name>/lay/<block-name>_top' and generate resulting"
    echo "    '<block-name>/out/<block-name>.mag' and '<block-name>/out/<block-name>.gds' files, creating the"
    echo "    '<block-name>/out/' folder as necessary overwriting all original contents."
    echo ""
    echo "EXAMPLE:"
    echo "    To generate 'ip/opamp/out/opamp.mag' and 'ip/opamp/out/opamp.gds' from 'ip/opamp/lay/opamp_top.mag' do:"
    echo "    \$out opamp"
    echo ""
    exit 1
fi

BLOCK_NAME=$1
if [ $# -lt 2 ]; then
    CELL_NAME=${1}_top
else
    CELL_NAME=$2
fi

echo "Block name : \""$BLOCK_NAME"\""
echo "Cell name  : \""$CELL_NAME"\""

LAY_PATH=$PROJECT_ROOT/ip/$BLOCK_NAME/lay/
OUT_PATH=$PROJECT_ROOT/ip/$BLOCK_NAME/out/

# check for the existence of the schematic and layout files
if [ ! -f $LAY_PATH/${CELL_NAME}.mag ]; then
    echo "No layout called \"$LAY_PATH/${CELL_NAME}.mag\"."
    exit
fi

mkdir $OUT_PATH
rm -r $OUT_PATH/*

cd $OUT_PATH

# invoke magic for layout generation
magic -rcfile $PDK_ROOT/libs.tech/magic/sky130A.magicrc -noconsole -dnull << EOF
load $LAY_PATH/${CELL_NAME}.mag
gds write ${CELL_NAME}_hard
flatten ${CELL_NAME}_flat
load ${CELL_NAME}_flat
save ${CELL_NAME}_hard
EOF
#flatten -nolabels ${CELL_NAME}_flat

echo "OUTPUT HERE:"
pwd
ls -lrt
