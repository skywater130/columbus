#!/bin/bash

if [ -z ${PROJECT_ROOT+x} ]; then
    echo "please source \"envsetup.sh\""
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "USAGE:"
    echo "    \$lvs [block-name [cell-name]]"
    echo ""
    echo "    To run LVS on the flattened output layout file omit the cell name from the arg. list. This will cause"
    echo "    the tool to run LVS on the schematic in 'sch/<block-name>_top.sch' and layout 'out/<block-name>.mag'."
    echo ""
    echo "EXAMPLE:"
    echo "    To run LVS on layout 'ip/opamp/lay/opamp_dp.mag' and schematic 'ip/opamp/sch/opamp_dp.sch' do:"
    echo "    \$lvs opamp opamp_dp"
    echo ""
    echo "    To run LVS on layout 'ip/opamp/out/opamp.mag' and schematic 'ip/opamp/sch/opamp_top.sch' do:"
    echo "    \$lvs opamp"
    echo ""
    echo "    To run LVS on layout 'ip/opamp/lay/opamp_top.mag' and schematic 'ip/opamp/sch/opamp_top.sch' do:"
    echo "    \$lvs opamp opamp_top"
    echo ""
    exit 1
fi

BLOCK_NAME=$1
if [ $# -lt 2 ]; then
    CELL_NAME=${1}_top_hard
    MAG_CELL=$PROJECT_ROOT/ip/$BLOCK_NAME/out/${CELL_NAME}_hard
else
    CELL_NAME=${2}
    MAG_CELL=$PROJECT_ROOT/ip/$BLOCK_NAME/lay/$CELL_NAME
fi

echo "Block name : \""$BLOCK_NAME"\""
echo "Cell name  : \""$CELL_NAME"\""

MAG_FILE=$MAG_CELL.mag
SCH_FILE=$PROJECT_ROOT/ip/$BLOCK_NAME/sch/$CELL_NAME.sch
TMP_PATH=$PROJECT_ROOT/tmp/lvs

echo "Schematic  : \""$SCH_FILE"\""
echo "Layout     : \""$MAG_FILE"\""
echo "Output     : \""$TMP_PATH"\""

mkdir -p $TMP_PATH

# check for the existence of the schematic and layout files
if [ ! -f $MAG_FILE ]; then
    echo "No layout called \"$MAG_FILE\"."
    exit
fi
if [ ! -f $SCH_FILE ]; then
    echo "No schematic called \"$SCH_FILE\"."
    exit
fi

cd $TMP_PATH
# clean the LVS folder
rm ../../tmp/lvs/*

# invoke magic for lvs spice netlist extraction
#magic -rcfile $PDK_ROOT/sky130A/libs.tech/magic/sky130A.magicrc -noconsole -dnull extract_lvs.tcl &
# TODO - use 'extresist' in magic for parasitic resistance extraction when it works - doesn't work at the moment
magic -rcfile $PDK_ROOT/libs.tech/magic/sky130A.magicrc -noconsole -dnull << EOF
load $MAG_CELL
extract all
ext2spice lvs
ext2spice -o ${CELL_NAME}_lvsmag.spice
exit
EOF
#load $MAG_CELL
#flatten -nolabels ${CELL_NAME}_flat
#load ${CELL_NAME}_flat
#extract all
#ext2spice lvs
#ext2spice -o ${CELL_NAME}_lvsmag.spice
#exit

# wait for lvs netlist to be generated
printf "Waiting for LVS extracted netlist to be generated.."
while [ ! -s ${CELL_NAME}_lvsmag.spice ]
    do
    printf "."
    sleep 0.25
done
echo " "

# invoke xschem for spice netlist generation
xschem --rcfile=$PROJECT_ROOT/xschemrc -n -q -o "$TMP_PATH" --tcl "set top_subckt 1" "$SCH_FILE"
mv $CELL_NAME.spice ${CELL_NAME}_lvssch.spice

#sed -i '$s,.end,.include '"$PDK_ROOT"'\/skywater-pdk/libraries/sky130_fd_pr/latest/models/sky130.lib.spice\n.end,g' "${CELL_NAME}_lvssch.spice"
echo "netgen -batch lvs \"${CELL_NAME}_lvsmag.spice $CELL_NAME\" \"${CELL_NAME}_lvssch.spice $CELL_NAME\" $PDK_ROOT/libs.tech/netgen/sky130A_setup.tcl lvs_report.log"
netgen -batch lvs "${CELL_NAME}_lvsmag.spice $CELL_NAME" "${CELL_NAME}_lvssch.spice $CELL_NAME" $PDK_ROOT/libs.tech/netgen/sky130A_setup.tcl lvs_report.log
#echo $?

echo "$TMP_PATH/lvs_report.log" > $PROJECT_ROOT/lastreport

echo "REPORT HERE: $TMP_PATH/lvs_report.log"

# FIXME hopefully the below can be removed and replaced with an automated pass/fail message later on
echo "View the report to see the results of the LVS check!!!!!!!!!"
echo "View the report to see the results of the LVS check!!!!!!!!!"
echo "View the report to see the results of the LVS check!!!!!!!!!"
echo "View the report to see the results of the LVS check!!!!!!!!!"
echo "View the report to see the results of the LVS check!!!!!!!!!"
echo "View the report to see the results of the LVS check!!!!!!!!!"
#echo "
#"
#if [[ $(grep -i "error" $TMP_PATH/lvs_report.log|wc -l) -ne 0 ]] || [[ $(grep -i "failed" $TMP_PATH/lvs_report.log|wc -l) -ne 0 ]]; then
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#elif [[ $(grep -i "Circuits match uniquely" $TMP_PATH/lvs_report.log|wc -l) -ne 0 ]] && [[ $(grep -i "Netlists match uniquely" $TMP_PATH/lvs_report.log|wc -l) -ne 0 ]]; then
#    echo "PASS!"
#else
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#    echo "FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED! FAILED!"
#fi
