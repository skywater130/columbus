#!/bin/bash

echo "USAGE:"
echo "    \$lay [block-name [cell-name]]"
echo ""
echo "    - Running with no arguments will open Magic in the current folder."
echo "    - Running with one argument will open Magic in the '<block-name>/lay' folder and open '<block-name>_top'."
echo "    - Running with two argument will open Magic in the '<block-name>/lay' folder and open '<cell-name>'."
echo ""
echo "EXAMPLE:"
echo "    To open layout 'ip/opamp/lay/opamp_dp.mag' do:"
echo "    \$lay opamp opamp_dp"
echo ""
echo "    To open layout 'ip/opamp/lay/opamp_top.mag' do:"
echo "    \$lay opamp opamp_top"
echo ""

# open top level layout view of provided cell if cell unspecified
if [ $# -lt 1 ]; then
    magic -d OGL -rcfile $PDK_ROOT/libs.tech/magic/sky130A.magicrc &
    exit 1
elif [ $# -lt 2 ]; then
    LAYOUT_DIR=$PROJECT_ROOT/ip/$1/lay
    LAYOUT_NAME=$1_top
else
    LAYOUT_DIR=$PROJECT_ROOT/ip/$1/lay
    LAYOUT_NAME=$2
fi

if [ ! -f $LAYOUT_DIR/$LAYOUT_NAME.mag ]; then
    echo "No layout called \"$LAYOUT_NAME\"... Creating it."
fi

pushd $LAYOUT_DIR
magic -d OGL -rcfile $PDK_ROOT/libs.tech/magic/sky130A.magicrc $LAYOUT_NAME &
popd
