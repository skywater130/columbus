#!/bin/bash

if [ -z ${PROJECT_ROOT+x} ]; then
    echo "please source \"envsetup.sh\""
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "USAGE:"
    echo "    \$pex <block-name> [cell-name]"
    echo ""
    echo "    To run LVS on the flattened output layout file omit the cell name from the arg. list. This will cause"
    echo "    the tool to run LVS on the schematic in 'sch/<block-name>_top.sch' and layout 'out/<block-name>.mag'."
    echo ""
    echo "EXAMPLE:"
    echo "    To run run PEX on layout 'ip/opamp/lay/opamp_dp.mag' do:"
    echo "    \$pex opamp opamp_dp"
    echo ""
    echo "    To run run PEX on layout 'ip/opamp/out/opamp.mag' do:"
    echo "    \$pex opamp"
    echo ""
    echo "    To run run PEX on layout 'ip/opamp/lay/opamp_top.mag' do:"
    echo "    \$pex opamp opamp_top"
    echo ""
    exit 1
fi

BLOCK_NAME=$1
if [ $# -lt 2 ]; then
    CELL_NAME=${1}_top_hard
    MAG_CELL=$PROJECT_ROOT/ip/$BLOCK_NAME/out/${1}
else
    CELL_NAME=${2}
    MAG_CELL=$PROJECT_ROOT/ip/$BLOCK_NAME/lay/$CELL_NAME
fi

echo "Block name : \""$BLOCK_NAME"\""
echo "Cell name  : \""$CELL_NAME"\""

MAG_FILE=$MAG_CELL.mag
TMP_PATH=$PROJECT_ROOT/tmp/pex
OUTPUT_FILE_NAME=${CELL_NAME}_pex.spice

echo "Layout     : \""$MAG_FILE"\""
echo "Output     : \""$TMP_PATH"\""

mkdir -p $TMP_PATH

# check for the existence of the schematic and layout files
if [ ! -f $MAG_FILE ]; then
    echo "No layout called \"$MAG_FILE\"."
    exit
fi

RUN_DIR=$PWD
cd $TMP_PATH
# clean the PEX folder
rm ../../tmp/pex/*

# invoke magic for parasitic extraction
#magic -rcfile $PDK_ROOT/sky130A/libs.tech/magic/sky130A.magicrc -noconsole -dnull extract_lvs.tcl &
magic -rcfile $PDK_ROOT/libs.tech/magic/sky130A.magicrc -noconsole -dnull << EOF
load $MAG_CELL
box values 0 0 0 0
extract all
ext2sim labels on
ext2sim
extresist tolerance 10
extresist
ext2spice lvs
ext2spice cthresh 0.01
ext2spice extresist on
ext2spice -o $OUTPUT_FILE_NAME
exit
EOF
#load $MAG_CELL
#flatten ${CELL_NAME}_pex
#load ${CELL_NAME}_pex
#box values 0 0 0 0
#extract all
#ext2sim labels on
#ext2sim
#extresist tolerance 10
#extresist
#ext2spice lvs
#ext2spice cthresh 0.01
#ext2spice extresist on
#ext2spice -o $OUTPUT_FILE_NAME
#exit

# wait for lvs netlist to be generated
printf "Waiting for PEX extracted netlist to be generated.."
while [ ! -s $OUTPUT_FILE_NAME ]
    do
    printf "."
    sleep 0.25
done
echo " "

echo "$TMP_PATH/$OUTPUT_FILE_NAME" > $PROJECT_ROOT/lastpex

echo "PEX NETLIST HERE: $TMP_PATH/$OUTPUT_FILE_NAME"
